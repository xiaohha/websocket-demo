import Vue from 'vue'
import Router from 'vue-router'
import ChatRoom from '@/components/ChatRoom'

Vue.use(Router)

export default new Router({
  routes: [
    {
      // 根路径默认到聊天室
      path: '/',
      name: 'ChatRoom',
      component: ChatRoom
    }
  ]
})
